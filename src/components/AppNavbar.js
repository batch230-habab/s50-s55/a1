import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {Link, NavLink} from 'react-router-dom';
import {useState, Fragment, useContext} from 'react'
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';


//! experimental components
/* import SvgIcon from 'material-ui/SvgIcon';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
function HomeIcon(props) {
    return (
    <SvgIcon {...props}>
        <path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z" />
    </SvgIcon>
    );
} */

//extracted from here https://react-bootstrap.github.io/components/alerts/
export default function AppNavbar(){
    //let [user, setUser] = useState(localStorage.getItem('email'));
    const { user } = useContext(UserContext);
    /* function logout(){window.location.replace("/login"); return(<Navigate to="/login" /> ) } */
    return (
        <Navbar bg="light" expand="lg" collapseOnSelect>
            <Container fluid>
                <Navbar.Brand as={Link} to="/">Zuitt Booking</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="mr-auto">
                        {/* <Nav.Link href="/">Home</Nav.Link> */}
                        <Nav.Link as={NavLink} to="/"  href="#home">Home</Nav.Link>
                        <Nav.Link as={NavLink} to="/courses"  href="#courses">Courses</Nav.Link>
                        {(user.id == null)?
                            <Fragment>
                                <Nav.Link as={NavLink} to="/login"  href="#login">Login</Nav.Link>
                            </Fragment>
                            
                            :
                            <Nav.Link as={NavLink} to="/logout"  href="#home">Logout {user.email}</Nav.Link>
                        }
                        <Nav.Link as={NavLink} to="/register"  href="#register">Register</Nav.Link>
                        </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}