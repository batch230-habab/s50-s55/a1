import { Button, Card } from 'react-bootstrap';
import { useState , useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'




export default function CourseCard({courseProp}){
    const { _id, name, description, price} = courseProp;
    return (
    <Card style={{ width: '100%' }}>
        <Card.Body>
            <Card.Title>Name: {name}</Card.Title>
            <h6>Description:</h6>
            <Card.Text>
                {description}
            </Card.Text>
            <h6>Price:</h6>
            <Card.Text>
                PhP {price}
            </Card.Text>
            <Button as={Link} to={`/courses/${_id}`}>Enroll</Button>
        </Card.Body>
    </Card>
    )
}