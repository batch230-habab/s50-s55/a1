import React from 'react';

// * creates a context object
//? A context object with object data type that can be used to store information that can be shared to other components within the application
const UserContext = React.createContext();
/* console.warn(`contents of UserContext: ${UserContext}`) */

//? The "Provider" component allows other components to consume/use the context object and supply the necessary information needed in the context object

// the provider is used to create a context that can be consumed by other components
export const UserProvider = UserContext.Provider;

export default UserContext;