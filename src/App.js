// import logo from './logo.svg';
import './App.css';
import AppNavbar from './components/AppNavbar';
import { useState, useEffect, useContext } from 'react';
import { Container } from 'react-bootstrap';

//? S53
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';


import NotFound from './pages/NotFound'
import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import { UserProvider } from './UserContext'
import Settings from './pages/Settings';
import UserContext from './UserContext'
import CourseView from './pages/CourseView'

function App() {
  let [user, setUser] = useState({
    email: localStorage.getItem('email'),
    id: localStorage.getItem('id'),
    isAdmin: localStorage.getItem('isAdmin')
  })

  const unsetUser = () =>{
    localStorage.clear();
  }
  return (
    <UserProvider value ={{user, setUser, unsetUser}}>
      <Router>
      <AppNavbar />
        <Container fluid>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/courses" element={<Courses />} />
            <Route path="/courses/:courseId" element ={<CourseView/>}/>
            <Route path="/register" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/settings" element={<Settings />} />
            <Route path="*" element={<NotFound />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
