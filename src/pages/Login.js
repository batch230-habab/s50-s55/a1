import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

//! password handling experiment
import IconButton from "@material-ui/core/IconButton";
import Visibility from "@material-ui/icons/Visibility";
import InputAdornment from "@material-ui/core/InputAdornment";
import Input from "@material-ui/core/Input";
import { VisibilityOffOutlined } from '@material-ui/icons';
//! end password handling experiment dependencies

export default function Login(){

    const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState(''),
    [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    //! password handling experiment :)
    const [showPassword, setShowPassword] = useState(false);
    const showPasswordHandler = () => {setShowPassword(!showPassword);}

    /* function login(e) {
        e.preventDefault();
        localStorage.setItem('email', email);
        localStorage.setItem('password', password);

        setUser({
            email: localStorage.getItem('email')
        })
        alert('login successful');
        window.location.reload();
    } */

    function login2(e){
        e.preventDefault();
        fetch('http://localhost:4000/users/login',{
            method: 'POST',
            headers: { 'Content-type' : 'application/json' },
            body: JSON.stringify({
                email: email,
                password: password
            })
        }).then(res =>res.json())
        .then(data => {
            console.log(data);
            console.log("Check accessToken");
            console.log(data.accessToken);

            if(typeof data.accessToken !== "undefined"){
                localStorage.setItem('token', data.accessToken);
                retrieveUserDetails(data.accessToken);
                Swal.fire({
                    title: "Login Successful",
                    icon: "success",
                    text: "Welcome to Zuitt!"
                })
            }else{
                Swal.fire({
                    title: "Authentication failed",
                    icon: "error",
                    text: "Check your login details and try again.",
                    timer: 10000
                })
            }
        })
        setEmail('');
    }
    
    const retrieveUserDetails = (token) => {
        fetch('http://localhost:4000/users/details',{
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res=>res.json())
        .then(data => {
            console.log(`this is from retrieve user details`);
            console.log(data);

            setUser({
                email: data.email,
                id: data._id,
                isAdmin : data.isAdmin
            })
            localStorage.setItem('id', data._id);
            localStorage.setItem('email', data.email);
        })

    }
    useEffect(()=>{
        if(email !== '' && password !== '') {
            setIsActive(true);
        }else{
            setIsActive(false);
        }
    }, [email, password])
    return (
        (user.id !== null)
            ?//true - means email field is not null or has a value
            <Navigate to="/courses" />
            ://false - means email field is not successfully set
        <div className="justify-content-center container-fluid align-items-center row d-flex text-center">
            <Form onSubmit={(e) => login2(e)}>
            <h3>Login</h3>
            <Form.Group controlId = "userEmail">
                <Input
                    placeholder = "Enter email"
                    type="email"
                    value = {email}
                    onChange = {e => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password" className="mb-3">
            <Input 
                placeholder="Enter your password"
                value={password}
                type={showPassword ? "text" : "password"}
                endAdornment={
                    <InputAdornment position="end">
                        <IconButton 
                        onClick={showPasswordHandler}
                        >{showPassword? <Visibility /> : <VisibilityOffOutlined/>}</IconButton>
                    </InputAdornment>
                }
                onChange = {e=>setPassword(e.target.value)}
            />

            </Form.Group>
            {isActive ?
                <Button variant="primary" type="submit" id="submitBtn">
                    Login
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Required fields are empty
                </Button>
            }
            </Form>
        </div>
    )
}


