import Banner from '../components/Banner'
export default function NotFound() {
    const data={
        title:"404 - Not found",
        content: <div>page you are looking for is not found<iframe src="https://thumbs.gfycat.com/AccurateUnfinishedBergerpicard.webp" width="600" height="600"/></div>,
        destination: "/",
        label: "home"
    }
    return(
        <Banner data={data}/>
    )
}