import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function Register(){
    //state hooks to store the values of input fields
    const [email, setEmail] = useState(''),
    [password2, setPassword2] = useState(''),
    [firstName, setFirstName] = useState(''),
    [lastName, setLastName] = useState(''),
    [mobileNumber, setMobileNumber] = useState(''),
    [password, setPassword] = useState('');

    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);
    // check if values are successfully bound
    /* console.log(`email : ${email}`);
    console.log(`password: ${password}`);
    console.log(`mobileNo: ${mobileNo}`);
    console.log(`Name: ${firstName} ${lastName}`); */
    /* function toLogin(){window.location.replace("/login"); return(<Navigate to="/login" />);} */

    const { user } = useContext(UserContext);

    async function registerUser(e){
        e.preventDefault();
        let result = await fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
            method: 'POST',
            body: JSON.stringify({firstName, lastName, email, password, mobileNumber}),
            headers: {'Content-Type': 'application/json'},
        })
        .catch(err=>console.log(err))
        result= await result.json();
        console.log(`fetch result`)
        console.log(result);
        (result)?
        Swal.fire({
            title:"Registration Successful!",
            icon: "success",
            text: "Please proceed to login",
            timer: 20000
        })
        :
        Swal.fire({
            title:"Registration Failed",
            icon:"error",
            text: "Email is already in use"
        })
    }
    useEffect(() => {
        if((email!=='' && 
        password !=='' && 
        password2 !=='' && 
        firstName!=='' && 
        lastName!=='' && 
        mobileNumber!=='')&& 
        (password === password2)){
            setIsActive(true);
        }else{
            setIsActive(false);
        }
    }, [email, password, password2, mobileNumber, firstName, lastName])

    return (
        (user.id !== null)?
            <Navigate to="/courses" />
            :
            <Form onSubmit={(e) => registerUser(e)}>
                <Form.Group controlId="userEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control 
                        type="email" 
                        placeholder="Enter email"
                        value = {email}
                        onChange = {e => setEmail(e.target.value)}
                        required
                    />
                    <Form.Text className="text-muted">
                        Email must be unique.
                    </Form.Text>
                </Form.Group>
                <Form.Group controlId="password1">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password"
                        placeholder="Password"
                        value= {password}
                        onChange = {e => setPassword(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="password2">
                    <Form.Label>Verify Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        placeholder="Verify Password"
                        value = {password2}
                        onChange = {e => setPassword2(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="firstName">
                    <Form.Label>First Name: </Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="First Name"
                        value = {firstName}
                        onChange = {e => setFirstName(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="lastName">
                    <Form.Label>Last Name: </Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Last Name"
                        value = {lastName}
                        onChange = {e => setLastName(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="mobileNumber">
                    <Form.Label>Mobile Number: </Form.Label>
                    <Form.Control 
                        type="text" 
                        placeholder="Mobile Number"
                        value = {mobileNumber}
                        onChange = {e => setMobileNumber(e.target.value)}
                        required
                    />
                </Form.Group>
                {isActive ?
                    <Button variant="primary" type="submit" id="submitBtn">
                    Submit
                    </Button>
                    :
                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                    Required fields are not satisfied
                    </Button>
                }
            </Form>    
    )
}